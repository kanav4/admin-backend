const express = require("express");
const cors = require("cors");
const users = require("./users.json");
const events = require("./events.json");
const bodyParser = require('body-parser')
const fs = require('fs');

const app = express();
app.use(bodyParser.json())

app.options('*', cors())

  const corsOptions = {
    AccessControlAllowOrigin: '*',
    AccessControlAllowCredentials : true,
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE'
  }
  app.use(cors(corsOptions))

app.use((req, res, next) => {
    // res.setHeader()
    res.addHeader("Access-Control-Allow-Origin", "*");
    next()
  })
app.get("/users",cors(), (req, res) => {
    res.json(users);
});
app.post("/createusers",cors(), (req, res) => {
    console.log("req", req.body)
    fs.writeFile('users.json', JSON.stringify(req.body), (err) => {
        // if (err) throw err;
        console.log('Data written to file');
    });
    res.send('Data retrieved and written to file');
    // res.json(users);
});

app.get("/events", (req, res) => {
    res.json(events);
});
app.post("/createevents", (req, res) => {
    console.log("req", req.body)
    fs.writeFile('events.json', JSON.stringify(req.body), (err) => {
        if (err) throw err;
        console.log('Data written to file');
    });
    res.send('Data retrieved and written to file');
    // res.json(events);
});


app.post("/updateevent", cors(),(req, res) => {
    console.log("req", req.body)
    fs.writeFile('events.json', JSON.stringify(req.body), (err) => {
        if (err) throw err;
        console.log('Data written to file');
    });
    res.send('Data retrieved and written to file');
    // res.json(events);
});

app.post("/deleteevent", cors(),(req, res) => {
    console.log("req", req.body)
    fs.writeFile('events.json', JSON.stringify(req.body), (err) => {
        if (err) throw err;
        console.log('Data written to file');
    });
     res.json(events);
    res.send('Data retrieved and written to file');
});


const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});